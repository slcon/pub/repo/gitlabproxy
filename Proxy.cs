﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;

namespace GitlabProxy
  {
  public class Proxy : IHttpHandler
    {
    Regex  pathMatcher = new Regex(@"^\/(?<project>.+?)\/-\/raw\/(?<commit>[\da-f].+?)\/(?<filepath>.+)$");
    string gitlabURL   = "{0}/api/v4/projects/{1}/repository/files/{2}/raw?ref={3}&private_token={4}";

    /*----------------------------------------------*/
    /* SSL connection will fail on IIS without      */
    /* this. It will work with IIS Express, though. */
    /* https://stackoverflow.com/a/2904963          */
    /*----------------------------------------------*/
    static Proxy()
      {
      System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls12;
      }

    /*-----------------------------------*/
    /* Handler can be cached and reused. */
    /*-----------------------------------*/
    public bool IsReusable { get => true; }

    /*---------------------*/
    /* Handle the request. */
    /*---------------------*/
    public void ProcessRequest(HttpContext context)
      {
      /*-----------------------------------*/
      /* Verify the URL is what we expect. */
      /*-----------------------------------*/
      var match = pathMatcher.Match(context.Request.Path);

      if (!match.Success)
        setStatus(context, 400, "Input URL did not match expected pattern.");
      else
        {
        /*---------------------------------------*/
        /* Parse the URL and verify we have a    */
        /* token for the project in AppSettings. */
        /*---------------------------------------*/
        var gitlabHost = ConfigurationManager.AppSettings["gitlabHost"];
        var project    = match.Groups["project"] .Value;
        var commit     = match.Groups["commit"]  .Value;
        var filepath   = match.Groups["filepath"].Value;

        /*----------------------------------------------------*/
        /* Search the appSettings to find the longest match   */
        /* to the project path and retrieve the access token. */
        /*----------------------------------------------------*/
        var keys       = ConfigurationManager.AppSettings.AllKeys.Where(k => project.StartsWith(k)).ToList();
        var key        = keys.Aggregate((string)null, (m, k) => (k.Length > (m??"").Length ? k : m)); 
        var token      = (key != null ? ConfigurationManager.AppSettings[key] : null);

        if (token == null)
          setStatus(context, 400, $"No token found for project '{project}'.");
        else
          {
          /*--------------------------------------*/
          /* Reformat the URL and pass to GitLab. */
          /*--------------------------------------*/
          HttpWebResponse response = null;
          try
            {
            try
              {
              var remoteUrl = string.Format(gitlabURL, gitlabHost, HttpUtility.UrlEncode(project), HttpUtility.UrlEncode(filepath), commit, token);
                  response  = WebRequest.Create(remoteUrl).GetResponse() as HttpWebResponse;
              }
            catch(WebException wex) when (wex.Response != null)
              {
              response = wex.Response as HttpWebResponse;
              }

            /*--------------------------------------*/
            /* Pass the reponse back to the client. */
            /*--------------------------------------*/
            context.Response.StatusCode        = (int)response.StatusCode;
            context.Response.StatusDescription = response.StatusDescription;
            context.Response.ContentType       = response.ContentType;
        
            var inStream  = response.GetResponseStream();
            var outStream = context.Response.OutputStream;
            var buffer    = new byte[1024];
            int bytes     = 0;
  
            while ((bytes = inStream.Read(buffer, 0, 1024)) > 0)
              outStream.Write(buffer, 0, bytes);
            }
          catch(Exception ex)
            {
            setStatus(context, 500, ex.Message);
            }
          }
        }
      }

    /*----------------------------------*/
    /* Writes error response to client. */
    /*----------------------------------*/
    void setStatus(HttpContext c, int code, string msg)
      {
      c.Response.StatusCode = code;
      c.Response.Write(msg);
      }
    }
  }