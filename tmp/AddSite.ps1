#*******************************************************************************
# Powershell script to add a TLS certificate, application pool, and website for
# the proxy server to the local IIS server.
#*******************************************************************************
param
  (
  [Parameter(Mandatory)][string]$siteName, # Website/AppPool name.
  [Parameter(Mandatory)][string]$sitePath, # Website physical path.
  [Parameter(Mandatory)][string]$hostName, # Website host name.
  [Parameter(Mandatory)][string]$certPath  # .p12 certificate file path.
  )

Import-Module IISAdministration
Import-Module PKI

$poolName = $siteName

#*-----------------------------------------------------------*
#* Install the cert to the Personal certs folder and convert *
#* the Certificate Hash (Thumbprint) from hex to byte array. *
#* https://stackoverflow.com/a/54543794/2245849              *
#*-----------------------------------------------------------*
$cert       = import-pfxcertificate $certPath 'Cert:\LocalMachine\My'
$thumbprint = $cert.Thumbprint
$certHash   = [byte[]] -split ($thumbprint -replace '..', '0x$& ')

#*-----------------------------------------------*
#* Install the CA certificate to the root store. *
#*-----------------------------------------------*
$cacertfile = New-TemporaryFile
Export-Certificate -FilePath $cacertfile.FullName -Cert (Get-PfxData $certPath).OtherCertificates[0] 
Import-Certificate -FilePath $cacertfile.FullName -CertStoreLocation 'Cert:\LocalMachine\Root'
Remove-Item        -Path     $cacertfile.FullName

#*----------------------------------------------------*
#* Get the Server Manager and configure the App Pool. *
#*----------------------------------------------------*
$manager    = Get-IISServerManager
$pool       = $manager.ApplicationPools[$poolName]

if ($pool -eq $null) {
  $pool = $manager.ApplicationPools.Add($poolName)
}
$pool.ManagedRuntimeVersion = "v4.0"

#*-------------------*
#* Rebuild the site. *
#*-------------------*
$site = $manager.Sites[$siteName]

if ($site -eq $null) {
  $site = $manager.Sites.Add($siteName, $sitePath, 80)
}

$site.Applications.Clear()
$app = $site.Applications.Add("/", $sitePath)
$app.ApplicationPoolName = $poolName

$site.Bindings.Clear()
$site.Bindings.Add("*:80:$hostName",  "http")
$site.Bindings.Add("*:443:$hostName", $certHash, "My", 1)

$manager.CommitChanges()
