;*******************************************************************************
;* Inno Setup Script for installing Gitlab Reverse Proxy server to IIS.
;* Installs a website on the local IIS server with a TLS certificate.
;* It does not modify the local `hosts` file. This will need to be done manually.
;*******************************************************************************
#define SiteName 'GitlabProxy'
#define HostName 'gitlabproxy.loc'
#define CertFile 'gitlabproxy.loc.p12'

[Setup]
AppName                   = "Gitlab Reverse Proxy Server"
AppVerName                = "GitLab Reverse Proxy Server"
DefaultDirName            = "C:\inetpub\wwwroot\{#SiteName}"
OutputBaseFilename        = "GitlabProxySetup"
OutputDir                 = .
AllowUNCPath              = no
CreateUninstallRegKey     = no
DirExistsWarning          = no
PrivilegesRequired        = admin
Uninstallable             = no
UpdateUninstallLogAppName = no
DisableDirPage            = no
DisableReadyPage          = no
AlwaysShowDirOnReadyPage  = yes
DisableProgramGroupPage   = yes
Compression               = lzma
SolidCompression          = yes

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Messages]
ClickFinish=Click Finish to exit Setup.

[InstallDelete]		     
Type: filesandordirs; Name: "{app}/*"             

[Files]
Source: Web.config; DestDir: {app};        Flags: ignoreversion
Source: bin/*.dll;  DestDir: {app}/bin;    Flags: ignoreversion
Source: tmp/*;      DestDir: {tmp}

[Files]
Source: ReadMe.txt; DestDir: {app};        Flags: ignoreversion
[Run]
Filename: "{app}\ReadMe.txt"; Description: "View the README file"; Flags: postinstall shellexec skipifsilent

;*------------------------------------------*
;* Install the certificate and the website. *
;*------------------------------------------*
[Run]
Filename: "powershell.exe"; \
  Parameters: "-ExecutionPolicy Bypass -File ""{tmp}\AddSite.ps1"" -SiteName ""{#SiteName}"" -SitePath ""{app}"" -HostName ""{#HostName}"" -CertPath ""{tmp}\{#CertFile}"""; \
  WorkingDir: {app}; Flags: runhidden

;*----------------------------------------------*
;* To view command execution in a window.       *
;* https://stackoverflow.com/a/53819553/2245849 *
;*----------------------------------------------*
;Filename: "{cmd}"; \
  Parameters: "/K ""powershell.exe -ExecutionPolicy Bypass -File """"{tmp}\AddSite.ps1"""" -SiteName """"{#SiteName}"""" -SitePath """"{app}"""" -HostName """"{#HostName}"""" -CertPath """"{tmp}\{#CertFile}"""""""; \
  WorkingDir: {app}

