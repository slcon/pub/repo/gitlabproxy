﻿GitLabProxy Configuration
=========================
The `Web.config` file `appSettings` section provides the service configuration.

<appSettings>
  <add key="gitlabHost"        value="https://gitlab.com" />
  <add key="mygroup/myproject" value="ProjectAccessToken" />
  <add key="mygroup"           value="GroupAccessToken" />
</appSettings>

In the example, `gitlabHost` identifies the GitLab repository host. All other
entries identify the `Project Path` of the GitLab project and the `Access Token`
required. If the project path identifies a GitLab group, all projects in the group 
can be accessed using the supplied token.

Visual Studio projects using Source Link with this proxy service must include this 
setting in the project file:

<ItemGroup>
  <SourceLinkGitLabHost Include="gitlab.com" ContentUrl="https://gitlabproxy.loc" />
</ItemGroup>

Complete documentation may be found at https://gitlab.com/slcon/pub/repo/gitlabproxy/-/blob/trunk/README.md.