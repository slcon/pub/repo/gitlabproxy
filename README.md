# GitlabProxy

IIS Reverse proxy server for translating Source Link URLs to GitLab API URLs. 

Debugging into a [NuGet Package] is facilitated using [Source Link]. 
The repository URLs of the package source code files are written into the 
`.pdb` file(s) so the Visual Studio Debugger can retrieve them. This works for 
NuGet packages built from public repositories. However, [GitLab does not currently 
provide authentication to a private repository through Source Link](https://github.com/dotnet/sourcelink/issues/281).

This is a reverse proxy service that will translate Source Link URLs, which have
no authentication capability, into GitLab API URLs, which do. 
<pre>
Source Link: https://gitlabproxy.loc/&lt;<b>ProjectPath</b>&gt;/-/raw/&lt;CommitHash&gt;/&lt;FilePath&gt;
GitLab API:  &lt;<b>GitlabHost</b>&gt;/api/v4/projects/&lt;<b>ProjectPath</b>&gt;/repository/files/&lt;FilePath&gt;/raw?ref=&lt;<b>AccessToken</b>&gt;
</pre>
Source Link must be configured to generate URLs with the host name of this proxy server.
When a request is received from the debugger, this service sends a request 
to the GitLab API with the translated URL and the response is returned seamlessly.

## Installation

The [Inno Setup] script file **`Installer.iss`** builds an installer that calls the
Powershell script **`tmp\AddSite.ps1`** which takes these parameters:

| Parameter Name | Installer Value |
| - | - |
| SiteName | `GitlabProxy` |
| SitePath | `C:\inetpub\wwwroot\GitlabProxy` |
| HostName | `gitlabproxy.loc` |
| CertPath | `tmp\gitlabproxy.p12` |

The script will create the IIS website on the target host, install the
service, and install the TLS certificate as well as the issuer CA certificate.

#### Manual Installation

The installation can be done manually by creating a website in the host IIS server, 
copying the service DLL file to the website `bin` folder, and manually setting the 
website **http(s)** bindings.

#### TLS Certificate

Securing the website with a TLS certificate is not strictly necessary since the proxy
will retreive the source code from GitLab securely, anyway. However, if the proxy service
only has an **http** binding, every source file that is retrieved from the repository must
be approved by the client. Here is an example of the Source Link dialog that appears when a 
file must be retrieved from the repository:

![Source Link Dialog](media/SourceLinkDialog.jpg)  

If the service is not secured with a TLS certificate, the option to skip the approval
for any subsequent requests does not appear.

## Configuration

  - The **Web.config** file **appSettings** section provides the service configuration.
```xml
<appSettings>
  <add key="gitlabHost"        value="https://gitlab.com" />
  <add key="mygroup/myproject" value="ProjectAccessToken" />
  <add key="mygroup"           value="GroupAccessToken" />
</appSettings>
```
In the example, **`gitlabHost`** identifies the GitLab repository host. All other
entries identify the **Project Path** of the GitLab project and the **Access Token**
required. If the project path identifies a GitLab group, all projects in the group 
can be accessed using the supplied token.

  - Visual Studio projects using Source Link with this proxy service must include this 
setting in the project file:

```xml
<ItemGroup>
  <SourceLinkGitLabHost Include="gitlab.com" ContentUrl="https://gitlabproxy.loc" />
</ItemGroup>
```

  - The local DNS server or the local **`hosts`** file will have to modified to point to the 
proxy service. The hosts file can be found in **`C:\Windows\System32\drivers\etc\hosts`**

## Notes

#### Source Link Deterministic URLs

[Source Link] maps local source file paths to repository URLs, by default:
```json
{
"documents": { "C:/MyLibrary/*":"https://gitlab.example.com/MyLibrary/-/raw/<CommitHash>/*" }
}
```
Visual Studio will first attempt to load the source code file by local path. This will work fine but
if you want to force the source files to be read from the repo, perform a [deterministic build](https://github.com/dotnet/sourcelink/blob/main/docs/README.md#continuousintegrationbuild) of the NuGet library to change the map:
```json
{
"documents": { "/_/*":"https://gitlab.example.com/MyLibrary/-/raw/<CommitHash>/*" }
}
```
Visual Studio will then always retrieve source code files from the repo.

#### "Go to definition" (F12)

Currently, it is not possible to navigate to the source code of a Source Link-enabled NuGet package
with the "Go to definition" option in Visual Studio. Source Link navigation only works in the
debugger. [But it looks like they are close to implementing this capability](https://github.com/dotnet/roslyn/issues/24349).


[NuGet Package]: https://docs.microsoft.com/en-us/nuget/what-is-nuget
[source link]:   https://docs.microsoft.com/en-us/dotnet/standard/library-guidance/sourcelink
[Inno Setup]:    https://jrsoftware.org/isinfo.php
